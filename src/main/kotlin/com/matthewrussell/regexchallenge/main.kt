package com.matthewrussell.regexchallenge

import com.matthewrussell.regexchallenge.csv.CSVFile
import com.matthewrussell.regexchallenge.dirnameparser.DirectoryNameParser
import java.io.File

fun main(args: Array<String>) {
    // Load the input CSV file
    val input = CSVFile.read(File(ClassLoader.getSystemResource("dir_names.csv").toURI()))

    // Process each dir name and keep the results
    val results = arrayListOf<DirectoryNameParser.Result>()
    for (row in input.rows) {
       results.add(DirectoryNameParser(row.first()).parse())
    }

    // Write the output CSV file
    val outputCsv = CSVFile()
    outputCsv.header.addAll(listOf("dir_name", "lang_code", "book_id", "resource_type", "level"))
    results.forEach { result ->
        outputCsv.rows.add(mutableListOf(
                result.dirName,
                result.language ?: "",
                result.book ?: "",
                result.resource  ?: "",
                result.level ?: ""
        ))
    }
    outputCsv.write(File("./results.csv"))
}