package com.matthewrussell.regexchallenge.dirnameparser

class DirectoryNameParser(private val dirName: String) {
    private val DIR_PATTERN = "(?:level_?|l)([0-9]+)|" +
            "(gen|exo|lev|num|deu|jos|jdg|rut|1sa|2sa|1ki|2ki|1ch|2ch|ezr|neh|est|job|psa|pro|ecc|sng|isa|" +
            "jer|lam|ezk|dan|hos|jol|amo|oba|jon|mic|nam|hab|zep|hag|zec|mal|mat|mrk|luk|jhn|act|rom|1co|2co|" +
            "gal|eph|php|col|1th|2th|1ti|2ti|tit|phm|heb|jas|1pe|2pe|1jn|2jn|3jn|jud|rev)|" +
            "(?:text_|bible_)?(reg|ulb|udb|notes|tn|tq|tw|obs){1}|uw|" +
            "([^-_][^_]+)"

    private val LEVEL_GROUP = 0
    private val BOOK_GROUP = 1
    private val RESOURCE_GROUP = 2
    private val LANGUAGE_GROUP = 3

    fun parse(): Result {
        val regex = Regex(DIR_PATTERN)
        // Get all the regex matches
        val matchResults = regex.findAll(dirName).toList()
        // Map to just get the group values
        val groupValues = matchResults.map { it.groupValues }

        // If a the match was for level, book, resource, or language
        // the corresponding element of the group values list for that match will
        // be non-null and contain the match result
        // Note that the first element in the group values list is the whole match
        // e.g. for level: [ level_2, 2, null, null, null]
        val parsedValues = arrayListOf<String>()
        for (groupIndex in 1..4) {
            // Find the list which has a non-null value for this groupIndex
            // Get that non-null value to get the value of the list
            parsedValues.add(
                    groupValues.filter { it[groupIndex].isNotEmpty() }.firstOrNull()?.get(groupIndex) ?: ""
            )
        }
        return Result(
                dirName,
                parsedValues[LANGUAGE_GROUP],
                parsedValues[BOOK_GROUP],
                parsedValues[RESOURCE_GROUP],
                parsedValues[LEVEL_GROUP]
        )
    }

    data class Result(
            val dirName: String = "",
            val language: String? = null,
            val book: String? = null,
            val resource: String? = null,
            val level: String? = null
    )
}