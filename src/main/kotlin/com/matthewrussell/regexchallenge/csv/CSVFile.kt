package com.matthewrussell.regexchallenge.csv

import java.io.File
import java.io.FileInputStream

class CSVFile {
    val header = mutableListOf<String>()
    val rows = mutableListOf<MutableList<String>>()

    companion object {
        fun read(file: File): CSVFile {
            val inputStream = FileInputStream(file)
            val reader = inputStream.bufferedReader()
            val result = CSVFile()
            result.header.addAll(reader.readLine().split(","))
            result.rows.addAll(reader.readLines().map { it.split(",").toMutableList() })
            return result
        }
    }

    fun write(file: File) {
        file.writeText(header.joinToString(",", postfix = "\n"))
        for (row in rows) {
            file.appendText(row.joinToString(",", postfix = "\n"))
        }
    }

}